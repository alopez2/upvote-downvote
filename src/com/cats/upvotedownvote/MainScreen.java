package com.cats.upvotedownvote;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainScreen extends Activity {

	ImageView upvote, downvote;
	Context ctx;
	TextView upvoteText, downvoteText;
	int upvotes, downvotes;
	String up, down;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_screen);
		ctx = this;
		upvoteText = (TextView) findViewById(R.id.upvotetext);
		downvoteText = (TextView) findViewById(R.id.downvotetext);
		upvote = (ImageView) findViewById(R.id.upvote);
		downvote = (ImageView) findViewById(R.id.downvote);
		upvotes = 0;
		downvotes = 0;
		up = "Upvotes: ";
		down = "Downvotes: ";
		upvoteText.setText(up + upvotes);
		downvoteText.setText(down + downvotes);
		
		upvote.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				upvotes++;
				upvoteText.setText(up + upvotes);
			}
		});
		
		downvote.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {				
				downvotes++;
				downvoteText.setText(down + downvotes);
			}
		});
	}
}
